;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (data stis-data src var-bv)
  #:use-module (data stis-data src var-mac)
  #:use-module (data stis-data atom)
  #:export (read-data-bv write-data-bv reg-set-var-bv reg-set-car-bv
                         reg-set-cdr-bv
			 reg-set-vector-bv reg-set-slot-bv reg-set-struct-bv
                         reg-set-hashq-bv reg-set-hash-bv))

(define-values (read-data-bv
		write-data-bv
		reg-set-var-bv
		reg-set-car-bv
		reg-set-cdr-bv
		reg-set-vector-bv
		reg-set-slot-bv
		reg-set-struct-bv
		reg-set-hashq-bv
		reg-set-hash-bv)
  
  (mk-var write-tag-bv read-tag-bv read-atom-bv write-atom-bv))
	  
     

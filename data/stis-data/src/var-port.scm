;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (data stis-data src var-port)
  #:use-module (data stis-data src var-mac)
  #:use-module (data stis-data atom)
  #:export (read-data write-data reg-set-var reg-set-car reg-set-cdr reg-set-vector reg-set-slot reg-set-struct reg-set-hashq reg-set-hash))

(mk-var read-data write-data write-tag read-tag read-atom write-atom
        reg-set-var reg-set-car reg-set-cdr reg-set-vector reg-set-slot
        reg-set-struct reg-set-hashq reg-set-hash)

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (data stis-data var)
  #:use-module (data stis-data atom)
  #:use-module (rnrs bytevectors)
  #:use-module (oop goops)
  #:export (mk-var rev-lookup lookup make-data init-data Data
		   register-named-object

		   write-data-bv
		   reg-set-var-bv
		   reg-set-car-bv
		   reg-set-cdr-bv
		   reg-set-vector-bv
		   reg-set-slot-bv
		   reg-set-struct-bv
		   reg-set-hashq-bv
		   reg-set-hash-bv

		   var-dump-bv
		   var-load-bv

		   ;;--------------------
		   write-data-p
		   reg-set-var-p
		   reg-set-car-p
		   reg-set-cdr-p
		   reg-set-vector-p
		   reg-set-slot-p
		   reg-set-struct-p
		   reg-set-hashq-p
		   reg-set-hash-p

		   var-dump-p
		   var-load-p
		   
		   ;;.........................
		   mk-var
		   mk-var-streamer
		   ))
  
#|
This code will track all objects with an id and handles all kinds of loops in 
the data staructures. It can also compress strings to the same value and the 
same is true for symbols and keywords. Example:

> (use-modules (data stis-data var)

> (define x (iota 100))

> (equal? x (data-load-bv (data-dumb-bv x)))

  #t


|#


#|
j Encoding
0 old vector
1 d x   atom
2 d i   var
3 d i   pair
5 d n a1 a2 ... an vector
6 d n a1 a2 ... an goops object
7 d n a1 a2 ... an struct object
8               an hashq object    
9               an hash  object
11 s i j val
12           set-vector
13           set-struct
14 0 s i val set-var
14 1 s i val car set-car
14 2 s i val cdr set-cdr
14 3 slot set
14 4 hashq set
14 5 hash  set
15 r i
|#

(define j-atom       1 )
(define j-var        2 )
(define j-pair       3 )
(define j-vector     5 )
(define j-instance   6 )
(define j-struct     7 )
(define j-hash-eq    8 )
(define j-hash-equal 9 )
(define j-ref        15)
(define j-set-cdr    16)
(define j-set-car    17)
(define j-set-vector 18)
(define j-set-var    19)
(define j-set-hashq  20)
(define j-set-hash   21)
(define j-set-slot   22)
(define j-set-struct 23)

(define (combine j n) (logior j (ash n 8)))

(define (atom? x)
  (or (number?     x)
      (string?     x)
      (symbol?     x)
      (boolean?    x)
      (char?       x)
      (keyword?    x)
      (bytevector? x)
      (null?       x)))
      


(define (register-equal i x obj)  
  (hash-set! (slot-ref obj 'HEQV) x i)
  (hash-set! (slot-ref obj 'HEQV) i x)) 

(define-syntax-rule (register-named-object c)
  (begin
    (set-object-property! c 'name 'c)
    (set-object-property! c 'dir  (module-name (current-module)))
    c))

(define (id++ i obj)
  (slot-set! obj 'n (max (+ i 1) (slot-ref obj 'n))))

(define (named-struct? x)
  (and
   (struct? x)
   (let ((v (struct-vtable x)))
     (object-property v 'name)
     (object-property v 'dir))))

(define-class Data () HEQ HEQV n compress?)
(define init-data
  (case-lambda
    ((obj comp)
     (slot-set! obj 'HEQ  (make-hash-table))
     (slot-set! obj 'HEQV (make-hash-table))
     (slot-set! obj 'n    0)
     (slot-set! obj 'compress? comp)
     obj)
    
    ((obj)
     (init-data obj #f))))

(define (make-data . x)
  (let ((obj (make Data)))
    (apply init-data obj x)))
	
(define (new-id obj)
  (let ((r (slot-ref obj 'n)))
    (slot-set! obj 'n (+ r 1))
    r))

(define (lookup v obj)
  (hashq-ref (slot-ref obj 'HEQ) v #f))

(define (lookup-atom v obj)
  (if (slot-ref obj 'compress?)
      (hash-ref  (slot-ref obj 'HEQV) v #f)
      (hashq-ref (slot-ref obj 'HEQ) v #f)))

(define (rev-lookup i obj)
  (hashq-ref (slot-ref obj 'HEQ) i #f))

(define (reg-obj r id obj)
  (id++ id obj)
  (hashq-set! (slot-ref obj 'HEQ)  r id)
  (hashq-set! (slot-ref obj 'HEQ)  id r))


(define (reg-atom s i obj)
  (id++ i obj)
  (if (slot-ref obj 'compress?)
      (hash-set!  (slot-ref obj 'HEQV) s i)
      (hashq-set! (slot-ref obj 'HEQ)  s i))    
  (hashq-set! (slot-ref obj 'HEQ) i s)
  s)


;; ========================== TOOL BOX ========================

(define (mk-var write-tag read-tag read-atom write-atom map)
  (let ()
  (define (reg-set-var bv i x v obj)
    (if (variable? x)
	(aif id (lookup x obj)
	     (-> (bv i)
		 (write-tag i-next (combine j-set-var id))
		 (write-data v obj))
	     (error "variable is not registred"))
	(error "not a variable")))

  (define (reg-set-car bv i x v obj)
    (if (pair? x)
	(aif id (lookup x obj)
	     (-> (bv i)
		 (write-tag i-next (combine j-set-car id))
		 (write-data v obj))
	     (error "pair is not registred"))
	(error "not a pair")))

  (define (reg-set-cdr bv i x v obj)
    (if (pair? x)
	(aif id (lookup x obj)
	     (-> (bv i)
		 (write-tag i-next (combine j-set-cdr id))
		 (write-data v obj))
	     (error "pair is not registred"))
	(error "not a pair")))

  (define (reg-set-vector bv i x j v obj)
    (if (vector? x)
	(aif id (lookup x obj)
	     (-> (bv i)
		 (write-tag i-next (combine j-set-vector id))
		 (write-data v obj))
	     (error "vector is not registred"))
	(error "not a vector")))

  (define (reg-set-hashq bv i x j k v obj)
    (if (hash-table? x)
	(aif id (lookup x obj)
	     (-> (bv i)
		 (write-tag i-next (combine j-set-hashq id))
		 (write-data k obj)
		 (write-data v obj))
	     (error "hashq is not registred"))
	(error "not a hash table")))

  (define (reg-set-hash bv i x k v obj)
    (if (hash-table? x)
	(aif id (lookup x obj)
	     (-> (bv i)
		 (write-tag i-next (combine j-set-hash id))
		 (write-data k obj)
		 (write-data v obj))
	     (error "hash is not registred"))
	(error "not a hashmap")))

  (define (reg-set-slot bv i x k v obj)
    (if (instance? x)
	(aif id (lookup x obj)
	     (-> (bv i)
		 (write-tag i-next (combine j-set-slot id))
		 (write-data k obj)
		 (write-data v obj))
	     (error "object is not registred"))
	(error "not a instance")))

  (define (reg-set-struct bv i x j v obj)
    (if (struct? x)
	(aif id (lookup x obj)
	     (-> (bv i)
		 (write-tag i-next (combine j-set-struct id))
		 (write-atom j)
		 (write-data v obj))
	     (error "struct is not registred"))
	(error "not a struct")))	   

  (define (write-data bv i cont x obj)
    (cond
     ((variable? x)
      (aif id (lookup x obj)
	   (write-tag bv i cont i-next (combine j-ref id))
	   
	   (let ((id (new-id obj))
		 (y  (variable-ref x)))
	     (reg-obj x id obj)
	     (-> (bv i cont)
		 (write-tag i-next (combine j-var id))
		 (write-data y obj)))))
 
     ((pair? x)
      (aif id (lookup x obj)
	   (write-tag bv i cont i-next (combine j-ref id))
	   (let ((id (new-id obj))
		 (y1 (car x))
		 (y2 (cdr x)))
	     (reg-obj x id obj)
	     (-> (bv i cont)
		 (write-tag  i-next (combine j-pair id))
		 (write-data y1 obj)
		 (write-data y2 obj)))))

     ((or (string? x) (symbol? x) (keyword? x))
      (aif id (lookup-atom x obj)
	 (write-tag bv i cont i-next (combine j-ref id))
	 
	 (let ((id (new-id obj)))
	   (reg-atom x id obj)
	   (-> (bv i cont)
	       (write-tag i-next (combine j-atom id))
	       (write-atom x)))))

     ((atom? x)
      (write-atom bv i cont x))
     
     ((hash-table? x)    
      (aif id (lookup x obj)
	   (write-tag bv i cont i-next (combine j-ref id))
	   
	   (let* ((l  (hash-fold (lambda (k v s) (cons (cons k v) s)) '() x))
		  (n  (length l))
		  (id (new-id obj)))
	     (reg-obj x id obj)
	     
	     (-> (bv i cont)
		 (write-tag i-next
			    (combine
			     (if (object-property x 'eq?)
				 j-hash-eq
				 j-hash-equal)
			     n))
		 
		 (write-atom n)
		 
		 ((lambda (bv i cont)
		    (let lp ((bv bv) (i i) (cont cont) (l l))
		      (if (pair? l)
			  (-> (bv i cont)
			      (write-data (car l) obj)
			      (write-data (cdr l) obj)
			      (lp (cdr l)))
			  (cont bv i)))))))))
     
     ((vector? x)
      (aif id (lookup x obj)
	   (write-tag bv i cont i-next (combine j-ref id))
	     
	   (let ((n (vector-length x))
		 (id (new-id obj)))
	     (reg-obj x id obj)
	     (-> (bv i cont)
		 (write-tag i-next (combine j-vector id))
		 (write-atom n)
		 ((lambda (bv i cont)
		    (let lp ((bv bv) (i i) (cont cont) (j 0))
		      (if (< j n)
			  (-> (bv i cont)
			      (write-data (vector-ref x j) obj)
			      (lp (+ j 1)))				       
			  (cont bv i)))))))))

     ((named-struct? x)
      (aif id (lookup x obj)
	   (write-tag bv i cont i-next (combine j-struct id))

	   (let* ((v    (struct-vtable x))
		  (name (object-property v 'name))
		  (dir  (object-property v 'dir))			
		  (n    (/ (string-length
			    (symbol->string (struct-layout x))) 2))
		  (id   (new-id obj)))

	     (reg-obj x id obj)

	     (-> (bv i cont)
		 (write-tag i-next (combine
				    (if (instance? x)
					j-instance
					j-struct)
				    n))

		 (write-atom n)
		 (write-data name obj)
		 (write-data dir  obj)

		 ((lambda (bv i cont)
		    (let lp ((bv bv) (i i) (cont cont) (j 0))
		      (if (< j n)
			  (-> (bv i cont)
			      (write-data (struct-ref x j) obj)
			      (lp (+ j 1)))
				       
			  (cont bv i)))))))))))
	   

  (define level (make-fluid 0))

  (for-each
   (lambda (x) (hash-set! map (car x) (cdr x)))
   (list
    (cons j-atom
	  (<=* (id obj)
	       (x (read-atom obj))
	       (<= (begin
		     (reg-atom x id obj)
		     x))))

    
    (cons j-var
	  (<=* (id obj)
	       (x  (read-atom obj))
	       (<= (let ((v (make-variable x)))
		     (reg-obj v id obj)
		     v))))
    (cons j-pair
	  (<=* (id obj)
	       (let ((x (cons #f #f)))
		 (*  (reg-obj x id obj))
		 (x1 (read-atom obj))
		 (x2 (read-atom obj))
		 (<= (begin
		       (set-car! x x1)
		       (set-cdr! x x2)
		       x)))))
        
    (cons j-vector
	  (<=* (id obj)
	       (n (read-atom obj))
	       (let ((v (make-vector n)))
		 (* (reg-obj v id obj))
		 (let lp ((j 0))
		   (if (< j n)
		       (begin
			 (x (read-atom obj))
			 (* (vector-set! v j x))
			 ((lp (+ j 1))))
		       (<= v))))))

    (cons j-instance
	  (<=* (id obj)
	       (n    (read-atom obj))
	       (name (read-atom obj))
	       (dir  (read-atom obj))
	       (let* ((m (resolve-module dir))
		      (v (module-ref m name))
		      (s (make v)))
		 (* (reg-obj s id obj))
		 (let lp ((j 0))
		   (if (< j n)
		       (begin
			 (x (read-atom obj))
			 (* (struct-set! s j x))
			 ((lp (+ j 1))))
		       (<= s))))))

    (cons j-struct
	  (<=* (id obj)
	       (n    (read-atom obj))
	       (name (read-atom obj))
	       (dir  (read-atom obj))
	       (let* ((m (resolve-module dir))
		      (v (module-ref m name))
		      (s ((struct-ref v 10)
			  (map (lambda (x) #f) (iota n)))))
		 (* (reg-obj s id obj))
		 (let lp ((j 0))
		   (if (< j n)
		       (begin
			 (x (read-atom obj))
			 (* (struct-set! s j x))
			 ((lp (+ j 1))))
		       (<= s))))))

    (cons j-hash-eq
	  (<=* (id obj)
	       (let ((h (make-hash-table)))
		 (* (reg-obj h id obj))
		 (n (read-atom obj))
		 (let lp ((j 0))
		   (if (< j n)
		       (begin
			 (k (read-atom obj))
			 (v (read-atom obj))
			 (* (hashq-set! h k v))
			 ((lp (+ j 1))))
		       (<= h))))))

    (cons j-hash-equal
     (<=* (id obj)
	  (let ((h (make-hash-table)))
	    (* (reg-obj h id obj))
	    (n (read-atom obj))
	    (let lp ((j 0))
	      (if (< j n)
		  (begin
		    (k (read-atom obj))
		    (v (read-atom obj))
		    (* (hash-set! h k v))
		    ((lp (+ j 1))))
		  (<= h))))))
		      
    (cons j-ref
	  (<=* (id obj)
	       (<= (rev-lookup id obj))))
    
    (cons j-set-struct
      (<=* (id obj)
	   (n   (read-atom obj))
	   (x   (read-atom obj))
	   (let ((s (rev-lookup id obj)))
	     (* (struct-set! s n x))
	     (<= s))))

    (cons j-set-slot
      (<=* (id obj)
	   (n   (read-atom obj))
	   (x   (read-atom obj))
	   (let ((s (rev-lookup id obj)))
	     (* (struct-set! s n x))
	     (<= s))))

    (cons j-set-var
	  (<=* (id obj)
	       (let ((v (rev-lookup id obj)))
		 (x (read-atom obj))
		 (* (variable-set! v x))
		 (<= v))))
    
    (cons j-set-car
	  (<=* (id obj)
	       (let ((v (rev-lookup id obj)))
		 (x (read-atom obj))
		 (* (set-car! v x))
		 (<= v))))

    (cons j-set-cdr
	  (<=* (id obj)
	       (let ((v (rev-lookup id obj)))
		 (x (read-atom obj))
		 (* (set-car! v x))
		 (<= v))))))
  
  (define* (var-load port #:optional (string-compress #t))
    (read-atom port 0 (lambda (bv i x) x)
	       (make-data string-compress)))

  (define* (var-dump x port #:optional (string-compress #t))
    (write-data port 0 (lambda (bv i) (values)) x
		(make-data string-compress)))

            	 
  (values write-data
	  reg-set-var
	  reg-set-car
	  reg-set-cdr
	  reg-set-vector
	  reg-set-slot
	  reg-set-struct
	  reg-set-hashq
	  reg-set-hash

	  var-dump
	  var-load)))

;; =================================== ISTANSATIONS ==========================
(define-values
    (write-data-bv
     reg-set-var-bv
     reg-set-car-bv
     reg-set-cdr-bv
     reg-set-vector-bv
     reg-set-slot-bv
     reg-set-struct-bv
     reg-set-hashq-bv
     reg-set-hash-bv

     var-dump-bv
     var-load-bv)

  (mk-var write-tag-bv read-tag-bv read-atom-bv write-atom-bv map-bv))

(define (mk-var-streamer get-u8 put-u8)
  (let ()
    (define-values (read-atom-p
		    write-atom-p
		    write-tag-p
		    read-tag-p
		    write-integer-p

		    map-p
		    
		    atom-dump-p
		    atom-load-p)
  
      (mk-streamer get-u8 put-u8))

    (mk-var write-tag-p read-tag-p read-atom-p write-atom-p map-p)))
  
    
(define-values
    (write-data-p
     reg-set-var-p
     reg-set-car-p
     reg-set-cdr-p
     reg-set-vector-p
     reg-set-slot-p
     reg-set-struct-p
     reg-set-hashq-p
     reg-set-hash-p

     var-dump-p
     var-load-p)
  
  (mk-var write-tag-p read-tag-p read-atom-p write-atom-p map-p))



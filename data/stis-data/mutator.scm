;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (data stis-data mutator)
  #:use-module (data stis-data var)
  #:use-module (oop goops)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 binary-ports)
  #:replace (variable-set!! vector-set! set-car! set-cdr! struct-set! slot-set!
			   variable-reg! car-reg! cdr-reg! vector-reg!
                           struct-reg! slot-reg! hashq-set! hash-set!)
  
  #:export (Mutator make-mutator *current-data* add-data store-mutator
		    get-mutator
		    
                    load-mutator-from-port 
		    load-mutator-from-x
		    bytevector->mutator

		    store-mutator-to-x
		    store-mutator-to-port
		    mutator->bytevector
		    
		    renew-mutator push-mutator store-mutator!

		    scm->mutator
		    mutator->scm
		    
		    loads
		    dumps))

(define *current-data* (make-fluid #f))

(define -slot-set!     (@ (oop goops) slot-set!))
(define -variable-set!! (@ (guile) variable-set!))
(define -set-car!      (@ (guile) set-car!))
(define -set-cdr!      (@ (guile) set-cdr!))
(define -vector-set!   (@ (guile) vector-set!))
(define -struct-set!   (@ (guile) struct-set!))
(define -hash-set!     (@ (guile) hash-set!))
(define -hashq-set!    (@ (guile) hashq-set!))

(define-syntax-rule (aif it p a b) (let ((it p)) (if it a b)))

(define-class Mutator (Data) bv i v)

(define (init-mutator v l)
  (let ((obj (make Mutator)))
    (-slot-set! obj 'bv (make-bytevector 1))
    (-slot-set! obj 'i  0)
    (-slot-set! obj 'v  v)
    (apply init-data obj l)
    obj))

(define (make-mutator v . l)
  (let ((obj (init-mutator v l)))
    (add-data obj v)
    obj))

(define (unpack obj lam)
  (let lp ((bv (slot-ref obj 'bv)) (i (slot-ref obj 'i)))
    (lam bv i)))

(define-method (add-data (obj Mutator) v)
  (unpack obj
    (lambda (bv i)
      (call-with-values
	  (lambda ()
	    (write-data-bv bv i values v obj))
	
	(lambda (bv i)
	  (-slot-set! obj 'bv bv)
	  (-slot-set! obj 'i  i)
	  obj)))))

(define push-mutator
  (case-lambda
    ((x)
     (let ((v (slot-ref (fluid-ref *current-data*) 'v)))
       (variable-set!! v (cons x (variable-ref v)))
       x))
    ((obj x)
     (let ((v (slot-ref obj 'v)))
       (variable-set!! obj v (cons x (variable-ref v)))
       x))))

(define (store-mutator-to-x mk put-u8)
  (lambda (obj)
    (lambda (port)
      (define (put-all port bv n)
	(let lp ((i 0))
	  (if (< i n)
	      (begin
		(put-u8 port (bytevector-u8-ref bv i))
		(lp (+ i 1)))
	      (put-u8 port (eof-object)))))
      
      (let ((port  (mk port))
	    (i     (slot-ref obj 'i)))
	(put-u8 port (if (slot-ref obj 'compress?) 1 0))
	(put-u8 port (logand (ash i -0 ) #xff))
	(put-u8 port (logand (ash i -8 ) #xff))
	(put-u8 port (logand (ash i -16) #xff))
	(put-u8 port (logand (ash i -24) #xff))
	(put-all port (slot-ref obj 'bv) (slot-ref obj 'i))))))

(define store-mutator-to-port
  (store-mutator-to-x (lambda (x) x) put-u8))

(define (consumer final)
  (lambda xx
    (let ((l '()))
      (lambda (byte)      
	(if (eof-object? byte)
	    (final l)
	    (set! l (cons byte l)))))))

(define (to-bv l)
  (let* ((n (length l))
	 (bv (make-bytevector n)))
    (let lp ((l l) (i (- n 1)))
      (if (>= i 0)
	  (let ((x (car l)))
	    (bytevector-u8-set! bv i x)
	    (lp (cdr l) (- i 1)))
	  bv))))

(define (consumer-put f x) (f x))

(define mutator->bytevector
  (lambda (obj)
    (( (store-mutator-to-x (consumer to-bv) consumer-put)
       obj)
     #f)))

(define (load-mutator-from-x mk get-u8)
  (lambda (port)
    (define (get-all port)
      (let lp ((l '()) (byte (get-u8 port)))
	(if (eof-object? byte)
	    (let* ((n  (length l))
		   (bv (make-bytevector n)))
	      (let lp ((l l) (i (- n 1)))
		(if (>= i 0)
		    (begin
		      (bytevector-u8-set! bv i (car l))
		      (lp (cdr l) (- i 1)))
		    bv)))
	    (lp (cons byte l) (get-u8 port)))))
      
    (let* ((port (mk port))
	   (c?   (if (= (get-u8 port) 1) #t #f))
	   (n    (logior
		  (ash (get-u8 port) 0)
		  (ash (get-u8 port) 8)
		  (ash (get-u8 port) 16)
		  (ash (get-u8 port) 24)))
	   
	   (bv   (let ((it (get-all port)))
		   (if (eof-object? it)
		       #()
		       it)))
	   
	   (obj  (make-data c?))
	   (i    (let lp ((i 0))
		   (if (< i n)
		       (call-with-values (lambda () (read-data-bv bv i obj))
			 (lambda (x i)
			   (lp i)))
		       n)))
	   (v    (rev-lookup 0 obj))
	   (out  (make Mutator)))
	
      (-slot-set! out 'n         (slot-ref obj 'n))
      (-slot-set! out 'HEQ       (slot-ref obj 'HEQ))
      (-slot-set! out 'HEQV      (slot-ref obj 'HEQV))
      (-slot-set! out 'compress? (slot-ref obj 'compress?))
      (-slot-set! out 'bv bv)
      (-slot-set! out 'i  i)
      (-slot-set! out 'v  v)
	
      out)))

(define load-mutator-from-port (load-mutator-from-x (lambda (x) x) get-u8))
(define (get-produce f) (f))
(define (bv-producer bv)
  (let ((i 0)
	(n (bytevector-length bv)))
    (lambda ()
      (if (< i (- n 1))
	  (let ((j i))
	    (set! i (+ i 1))
	    (bytevector-u8-ref bv i))	  
	  (eof-object)))))

(define bytevector->mutator
  (load-mutator-from-x bv-producer get-produce))

(define (store-mutator! . l)
  (let ((o (apply store-mutator l)))
    (fluid-set! *current-data* o)))

(define get-mutator
  (case-lambda
    ((o) (slot-ref o 'v))
    (() (get-mutator (fluid-ref *current-data*)))))

(define renew-mutator
  (case-lambda    
    (()
     (let ((m (fluid-ref *current-data*)))
       (fluid-set! *current-data* (make-mutator (slot-ref m 'v)))))
    ((m)
     (make-mutator (slot-ref m 'v)))))
   
(define-method (variable-reg! (obj Mutator) v x)
  (unpack obj
    (lambda (bv i)
      (call-with-values (lambda () (reg-set-var-bv bv i v x obj))
	(lambda (bv i)
	  (-slot-set! obj 'bv bv)
	  (-slot-set! obj 'i  i)))))
  (-variable-set!! v x))

(define variable-set!!
  (case-lambda
   ((obj v x)
    (variable-reg! obj v x))

   ((v x)
    (aif it (fluid-ref *current-data*)
	 (variable-set!! it v x)
	 (error "no current data set")))))

(define-method (car-reg! (obj Mutator) v x)
  (unpack obj
    (lambda (bv i)
      (call-with-values (reg-set-car-bv bv i v x obj)
	(lambda (bv i)
	  (-slot-set! obj 'bv bv)
	  (-slot-set! obj 'i  i)))))
  (-set-car! v x))

(define set-car!
  (case-lambda
    ((obj v x) (car-reg! obj v x))
    ((v x)
     (aif it (fluid-ref *current-data*)
	  (set-car! it v x)
	  (error "no current data set")))))

(define-method (cdr-reg! (obj Mutator) v x)
  (unpack obj
    (lambda (bv i)
      (call-with-values (reg-set-car-bv bv i v x obj)
	(lambda (i)
	  (-slot-set! obj 'bv bv)
	  (-slot-set! obj 'i  i)))))
  (-set-cdr! v x))

(define set-cdr!
  (case-lambda
    ((obj v x) (cdr-reg! obj v x))
    ((v x)
     (aif it (fluid-ref *current-data*)
	  (set-cdr! it v x)
	  (error "no current data set")))))

(define-method (vector-reg! (obj Mutator) v j x)
  (unpack obj
    (lambda (bv i)
      (call-with-values (reg-set-vector-bv bv i v j x obj)
	(lambda (bv i)
	  (-slot-set! obj 'bv bv)
	  (-slot-set! obj 'i  i)))))
  (-vector-set! v j x))

(define vector-set!
  (case-lambda
    ((obj v i x) (vector-reg! obj v x))
    ((v i x)
     (aif it (fluid-ref *current-data*)
	  (vector-set! it v i x)
	  (error "no current data set")))))

(define-method (struct-reg! (obj Mutator) v j x)
  (unpack obj
    (lambda (bv i)
      (call-with-values (reg-set-struct-bv bv i v j x obj)
	(lambda (bv i)
	  (-slot-set! obj 'bv bv)
	  (-slot-set! obj 'i  i)))))
  (-struct-set! v j x))

(define struct-set!
  (case-lambda
    ((obj v i x) (struct-reg! obj v x))
    ((v i x)
     (aif it (fluid-ref *current-data*)
	  (struct-set! it v i x)
	  (error "no current data set")))))

(define-method (slot-reg! (obj Mutator) v j x)
  (unpack obj
    (lambda (bv i)
      (call-with-values (reg-set-slot-bv bv i v j x obj)
	(lambda (bv i)
	  (-slot-set! obj 'bv bv)
	  (-slot-set! obj 'i  i)))))
  (-slot-set! v j x))

(define slot-set!
  (case-lambda
    ((obj v i x) (slot-reg! obj v i x))
    ((v i x)
     (aif it (fluid-ref *current-data*)
	  (slot-set! it v i x)
	  (error "no current data set")))))

(define-method (hashq-reg! (obj Mutator) v j x)
  (unpack obj
    (lambda (bv i)
      (call-with-values (reg-set-hashq-bv bv i v j x obj)
	(lambda (bv i)
	  (-slot-set! obj 'bv bv)
	  (-slot-set! obj 'i  i)))))
  (-hashq-set! v j x))

(define hashq-set!
  (case-lambda
    ((obj v i x) (hashq-reg! obj v i x))
    ((v i x)
     (aif it (fluid-ref *current-data*)
	  (hashq-set! it v i x)
	  (error "no current data set")))))

(define-method (hash-reg! (obj Mutator) v j x)
  (unpack obj
    (lambda (bv i)
      (call-with-values (reg-set-hash-bv bv i v j x obj)
	(lambda (bv i)
	  (-slot-set! obj 'bv bv)
	  (-slot-set! obj 'i  i)))))
  (-hash-set! v j x))

(define hash-set!
  (case-lambda
    ((obj v i x) (hash-reg! obj v i x))
    ((v i x)
     (aif it (fluid-ref *current-data*)
	  (hash-set! it v i x)
	  (error "no current data set")))))


(let* ((v   (make-variable '()))
       (obj (init-mutator v '())))
  (fluid-set! *current-data* obj)
  (add-data obj v))

(define (scm->mutator scm)
  (let* ((v   (make-variable '()))
	 (obj (init-mutator v '())))    
    (add-data obj v)
    (with-fluids ((*current-data* obj))
      (variable-set!! v scm))
    
    obj))
  
(define (mutator->scm obj)
  (variable-ref (slot-ref obj 'v)))

(define (loads bv)
  (mutator->scm (bytevector->mutator bv)))

(define (dumps scm)
  (mutator->bytevector (scm->mutator scm)))

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (data stis-data atom)
  #:use-module (rnrs bytevectors)
  #:use-module ((rnrs io ports) #:renamer
		(lambda (s)

		  (cond
		   ((eq? s 'bytevector->string) 'bytevector->string-2)
		   ((eq? s 'string->bytevector) 'string->bytevector-2)
		   (else
		    s))))
  
  #:use-module (ice-9 iconv)
  #:export (mk -> <- <=o <=* aif i-next
	       
	       ;; Bytevector streams
	       read-atom-bv
	       write-atom-bv
	       write-tag-bv
	       read-tag-bv
	       write-integer-bv
	       
	       map-bv
	       
	       atom-dump-bv
	       atom-load-bv
	       
	       ;; Port streams
	       read-atom-p
	       write-atom-p
	       write-tag-p
	       read-tag-p
	       write-integer-p

	       map-p

	       mk-streamer
	       
	       atom-dump-p
	       atom-load-p))

#|

This is a serializing tool to and from streams we also define a port stream 
and a bytevector stream interface. The scheme data structures can't be self
referential and no compression is done for similar tags.

Example:
> (use-modules (data stis-data atom))

> (define x (iota 100))

> (deifne y (atom-load-bv (atom-write-bv x)))

> (equal? x y)
 #t

|#


#|
0  #t
1  #f
2  double
3  single
4  +integer
5  -integer
6  bytevector
7  string  (utf-8)
8  symbol  (utf-8)
9  keyword (utf-8)
10 character
11 rational
12 complex(+ x1 (* x2 0+1i))
13 pair
14 vector
15 null
|#

(define i-int+     0 )
(define i-int-     1 )
(define i-bv       2 )
(define i-str      3 )
(define i-sym      4 )
(define i-keyw     5 )
(define i-char     6 )
(define i-ref      7 )
(define i-vector   8 )
(define i-bigint   9 )
(define i-imp      10)
(define i-next     15)

(define j-null     1 )
(define j-rational 2 )
(define j-complex  3 )
(define j-pair     4 )
(define j-next     5 )
(define j-double   6 )
(define j-single   7 )
(define j-true     8 )
(define j-false    9 )
(define j-next     10)

(define-syntax-rule (aif it p a b) (let ((it p)) (if it a b)))

#|
A little stream utility where we functionally would like to 
put values in the bytevector bv starting at position i

So every function f1,f2,... in this utility,  takes arguments

(f1 bv i cont args1 ...), (f2 bv i cont args1 ...), ...

and all calls cont with the new return position in the bytevector

(-> (bv i cont) (f1 arg1) (f2 arg2)) 

translates to

(f1 bv i (lambda (i) (f2 bv i cont arg2) arg1))

We have also the following short-cut,
(-> (bv i cont) (f1 arg1) (f2 arg2)) 

meaning

(f1 bv i (lambda (i) (f2 bv i (lambda (i) i) arg2) arg1))

It's a clean interface, but the drawback is that we can blow up the stack for
large datastructures. Now guile is resistant to this but indeed it is possible
to restruct the tool with a continuation in stead
|#
;; This is a similar tool using continuation passing style


(define-syntax ->
  (syntax-rules (if)
    ((_ (bv i) . l)
     (-> (bv i (lambda (bv i) (values bv i)))  . l))
    
    ((_ a #f)
     (-> a))

    ((_ a (if p x y))
     (if p (-> a x) (-> a y)))
    
    ((_ (bv i cont))
     (cont bv i))
     
    ((_ (bv i cont) (p a ...) pp ...)
     (let ((cont2 (lambda (bv i) (-> (bv i cont) pp ...))))
       (p bv i cont2 a ...)))))

(define-syntax <-
  (syntax-rules (let aif if let* begin <= *)
    ((_ (bv i cont) . l)
     (<- (bv i cont #f) . l))
    
    ((_ (bv i cont x ...)) (cont bv i x ...))

    ((_ args (if p x y))
     (if p (<- args x) (<- args y)))

    ((_ args (aif it p x y))
     (aif it p (<- args x) (<- args y)))

    ((_ args (begin . x))
     (<- args . x))

    ((_ (bv i cont . r) (<= . x))
     (cont bv i . x))
    
    ((_ args (let (a ...) . l))
     (let (a ...) (<- args . l)))

    ((_ (bv i cont . u) (let lp (a ...) . v))
     (let lp ((bv bv) (i i) (cont cont) a ...)
       (<- (bv i cont . u) . v)))

    ((_ args (let* a . l))
     (let* a (<- args . l)))

    ((_ args (* . l) . u)
     (begin
       (begin . l)
       (<- args . u)))

    ((_ (bv i cont x ...) ((f a ...)))
     (f bv i cont a ...))
    
    ((_ (bv i cont x ...) (x0 xx ... (f fa ...)) . l)
     (f bv i (lambda (bv i x0 xx ...)
	       (let* ((x0 x0) (xx xx) ...)
		 (<- (bv i cont x0) . l)))
	fa ...))))

(define-syntax-rule (<=o . l) (lambda (bv i cont) (<- (bv i cont) . l)))
(define-syntax-rule (<=* (x ...) . l) (lambda (bv i cont x ...)
					(<- (bv i cont) . l)))

;;============================= TOOLBOX ==============================

(define (mk read-u8
	    read-u16
	    read-u32
	    read-u64
	    read-single
	    read-double	        
            read-copy
			
	    write-u8
	    write-u16
	    write-u32
	    write-u64
	    write-single
	    write-double)
  (let ()
  (define (read-int n)    
    (cond
     ((= n 1)
      read-u8)
     
     ((= n 2)
      read-u16)
     
     ((= n 4)
      read-u32)

     ((= n 8)
      read-u64)
     
     (else
      (lambda (bv i cont)
	(let lp ((bv bv) (i i) (cont cont) (j 0) (b 0) (s 0))
	  (if (< j n)
	      (<- (bv i cont)
		  (x (read-u8))
		  ((lp (+ j 1)
		       (+ b 8)
		       (logior s (ash x b)))))
	      (cont bv i s)))))))


  (define (get-n bv i cont m)
    (cond
     ((= m 12)
      (read-u8  bv i cont))
     ((= m 13)
      (read-u16 bv i cont))
     ((= m 14)
      (read-u32 bv i cont))
     ((= m 15)
      (read-u64 bv i cont))
     (else
      (cont bv i m))))

     
  (define read-tag
    (<=o  (x (read-u8))
	  (let* ((t (logand x #xf))
		 (n (ash x -4)))
	    (<= t n))))

  (define (bytevector->str  x) (bytevector->string x "UTF-8"))
  (define (bytevector->sym  x)
    (string->symbol
     (bytevector->str x)))
  
  (define (bytevector->keyw x)
    (symbol->keyword
     (bytevector->sym x)))
		   
  (define (mk-read-bytes-x final)    
    (<=* (n obj)
	 (N  (get-n n))
	 (let ((v (make-bytevector N)))
	   (X  (read-copy v N))
	   (<= (final v)))))
  
  (define map  (make-hash-table))
  (define map2 (make-hash-table))
  (define map3 (make-hash-table))
  
  (for-each
   (lambda (f)
     (hash-set! map (car f) (cdr f)))

   (list            
    (cons i-int+
	  (<=* (n obj)
	       (N (get-n n))
	       (i ((read-int N)))
	       (<= i)))

    (cons i-int-
	  (<=* (n obj)
	       (N (get-n n))
	       (i ((read-int N)))
	       (<= (- i))))
    
    (cons i-bv   (mk-read-bytes-x (lambda (x) x)))
    (cons i-str  (mk-read-bytes-x bytevector->str))
    (cons i-sym  (mk-read-bytes-x bytevector->sym))
    (cons i-keyw (mk-read-bytes-x bytevector->keyw))

    
    (cons i-char
	  (<=* (n obj)
	       (N  (get-n n))
	       (x  ((read-int n)))
	       (<= (integer->char x))))
		    
    (cons i-vector
	  (<=* (n obj)
	       (N (get-n n))
	       (((lambda (bv i cont)
		   (let ((v (make-vector N)))
		     (let lp ((bv bv) (i i) (cont cont) (j 0))
		       (if (< j N)
			   (<- (bv i cont)
			       (x  (read-atom obj))
			       (*  (vector-set! v j x))
			       ((lp (+ j 1))))
			   v))))))))

    (cons i-next
	  (<=* (n obj)
	       (((hash-ref map2 n) obj))))))
  
  (for-each
   (lambda (f)
     (hash-set! map2 (car f) (cdr f)))
   (list
    (cons j-true
	  (<=* (obj) (<= #t)))
        
    (cons j-false
	  (<=* (obj) (<= #f)))

    (cons j-rational
	  (<=* (obj)   
	       (x  (read-atom obj))
	       (y  (read-atom obj))
	       (<= (/ x y))))

    (cons j-complex
	  (<=* (obj)   
	       (x  (read-atom obj))
	       (y  (read-atom obj))
	       (<= (+ x (* y 0+1i)))))

    (cons j-double
	  (<=* (obj) ((read-double))))
        
    (cons j-single
	  (<=* (n) ((read-single))))

    (cons j-pair
	  (<=* (obj)   
	       (x  (read-atom obj))
	       (y  (read-atom obj))
	       (<= (cons x y))))

    (cons j-null
	  (<=* (obj) (<= '())))

    (cons j-next
	  (<=* (obj)	       
	       (tag (read-u8))
	       (((hash-ref map3 tag) obj))))))
	       
  (define read-atom
    (<=* (obj)
	 (tag n (read-tag))
	 (((hash-ref map tag) n obj))))
  

  ;; ======================================================  Write toolbox
  (define (write-tag bv i cont tag n)
    (cond
     ((< n 12)
      (write-u8 bv i cont (logior tag (ash n 4))))
	   
     ((< n #xff)
      (-> (bv i cont)
	    (write-u8 (logior tag (ash 12 4)))
	    (write-u8 (logior tag n))))
	   	   
     ((< n #xffff)
      (-> (bv i cont)
	  (write-u8  (logior tag (ash 13 4)))
	  (write-u16 n)))

     ((< n #xffffffff)
      (-> (bv i cont)
	  (write-u8  (logior tag (ash 14 4)))	  
	  (write-u32 n)))
     
     (else
      (-> (bv i cont)
	  (write-u8  (logior tag (ash 15 4)))	  
	  (write-u64 n)))))
  


  (define (write-integer bv i cont tag x)
    (cond
     ((<= x #xff)      
      (-> (bv i cont)
	  (write-tag tag 1)
	  (write-u8 x)))
	   	   
     ((<= x #xffff)
      (-> (bv i cont)
	  (write-tag tag 2)
	  (write-u16 x)))
     
     ((<= x #xffffffff)
      (-> (bv i cont)
	  (write-tag tag 4)
	  (write-u32 x)))
	   	   
     ((<= x #xffffffffffffffff)
      (-> (bv i cont)
	  (write-tag tag 8)
	  (write-u64 x)))
     
     (else
      (let ((n (ceiling-quotient (integer-length x) 8)))
	(-> (bv i cont)
	    (write-tag tag n)
	    ((lambda (bv i cont)
	       (let lp ((bv bv) (i i) (cont cont) (j 0) (x x))
		 (let ((xx (logand x #xff)))
		   (-> (bv i cont)
		       (write-u8 xx)
		       (if (> x #xff)
			   (lp (+ j 1) (ash x -8))
			   #f)))))))))))
  
  (define (write-positive-integer bv i cont x)
    (write-integer bv i cont 4 x))

  (define (write-negative-integer bv i cont x)
    (write-integer bv i cont 4 (- x)))
  
  (define (write-double-data bv i cont x)
    (-> (bv i cont)
	(write-tag 15 j-double)
	(write-double)))

  (define (write-single-data bv i cont x)
    (-> (bv i cont)
	(write-tag 15 j-single)
	(write-single)))
	   
  (define (write-true bv i cont)
    (write-tag bv i cont 15 j-true))

  (define (write-false bv i cont)
    (write-tag bv i cont 15 j-false))
  
  (define (write-bytevector0 bv i cont tag b)
    (let* ((n (bytevector-length b)))
      (-> (bv i cont)
	  (write-tag tag n)
	  ((lambda (bv i cont)      
	     (let lp ((bv bv) (i i) (cont cont) (j 0))
	       (if (< j n)
		   (-> (bv i cont)
		    (write-u8 (bytevector-u8-ref b j))
		    (lp (+ j 1)))
		   (cont bv i))))))))

  (define (write-vector bv i cont x)
    (let* ((n (vector-length x)))
      (-> (bv i cont)
	  (write-tag i-next (logand j-vector (ash n 8)))
	  ((lambda (bv i cont)      
	     (let lp ((bv bv) (i i) (cont cont) (j 0))
	       (if (< j n)
		   (-> (bv i cont)
		       (write-atom (vector-ref x j))
		       (lp (+ j 1)))
		   (cont bv i))))))))
	   

  (define (write-bytevector bv i cont b)
    (write-bytevector0 bv i cont 6 bv))
  
  (define (write-string0 tag bv i cont x)
    (write-bytevector0 bv i cont tag
		       (string->bytevector x "UTF-8" 'error)))
  

  (define (write-string bv i cont x)
    (write-string0 7 bv i cont x))

  (define (write-symbol bv i cont x)
    (write-string0 8 bv i cont (symbol->string x)))

  (define (write-keyword bv i cont x)
    (write-string0 9 bv i cont (symbol->string (keyword->symbol x))))

  (define (write-character bv i cont x)
    (write-integer bv i cont 10 (char->integer x)))

  (define *double* #t)

  (define (write-atom bv i cont x)
    (cond
     ((number? x)
      (if (exact? x)
	  (cond
	   ((integer? x)
	    (if (>= x 0)
		(write-positive-integer bv i cont x)
		(write-negative-integer bv i cont x)))

	   ((rational? x)	     
	    (-> (bv i cont)
		(write-tag 15 j-rational)
		(write-atom (numerator x))
		(write-atom (denominator x)))))
	   
	  (cond     
	   ((real? x)
	    (if *double*
		(write-double bv i cont x)
		(write-single bv i cont x)))
	   
	   (else
	    (-> (bv i cont)
		(write-tag 15 j-complex)
		(write-atom (real-part x))
		(write-atom (imag-part x)))))))

     ((null? x)
      (write-tag bv i cont 15 j-null))
   
     ((pair? x)
      (-> (bv i cont)
	  (write-tag 15 j-pair)
	  (write-atom (car x))
	  (write-atom (cdr x))))
   
     ((string? x)
      (write-string bv i cont x))

     ((symbol? x)
      (write-symbol bv i cont x))

     ((boolean? x)
      (if x
	  (write-true  bv i cont)
	  (write-false bv i cont)))

     ((bytevector? x)
      (write-bytevector bv i cont x))

     ((vector? x)
      (write-vector bv i cont x))
    
     ((char? x)
      (write-character bv i cont x))
     
     ((keyword? x)
      (write-keyword bv i cont x))))

  (values read-atom write-atom write-tag read-tag write-integer map2)))

;; ======================= INSTANSIATIONS ==========================
(define endian 'little)

(define (checkafy0 n set)
  (lambda (bv i cont x)    
    (let ((N (bytevector-length bv)))
      (if (<= (+ i n) N)
	  (begin
	    (set bv i x)
	    (cont bv (+ i n)))
	
	  (let ((bv2 (make-bytevector (* 4 N))))
	    (bytevector-copy! bv 0 bv2 0 i)
	    (set bv2 i x)
	    (cont bv2 (+ i n)))))))

(define (checkafy n set)
  (lambda (bv i cont x)    
    (let ((N (bytevector-length bv)))
      (if (<= (+ i n) N)
	  (begin
	    (set bv i x endian)
	    (cont bv (+ i n)))
	
	  (let ((bv2 (make-bytevector (* 4 N))))
	    (bytevector-copy! bv 0 bv2 0 i)
	    (set bv2 i x endian)
	    (cont bv2 (+ i n)))))))

(define write-u8-bv     (checkafy0 1 bytevector-u8-set!))
(define write-u16-bv    (checkafy  2 bytevector-u16-set!))
(define write-u32-bv    (checkafy  4 bytevector-u32-set!))
(define write-u64-bv    (checkafy  8 bytevector-u64-set!))
(define write-single-bv (checkafy  4 bytevector-ieee-single-set!))
(define write-double-bv (checkafy  8 bytevector-ieee-double-set!))


(define read-u8-bv
  (lambda (bv i cont)
    (cont bv (+ i 1) (bytevector-u8-ref bv i))))

(define (mk-read-bv ref n)
  (lambda (bv i cont)
    (cont bv (+ i n) (ref bv i endian))))

(define read-u16-bv    (mk-read-bv bytevector-u16-ref 2))
(define read-u32-bv    (mk-read-bv bytevector-u32-ref 4))
(define read-u64-bv    (mk-read-bv bytevector-u64-ref 8))
(define read-single-bv (mk-read-bv bytevector-ieee-single-ref 4))
(define read-double-bv (mk-read-bv bytevector-ieee-double-ref 8))

(define (read-copy-bv bv i cont v n)
  (bytevector-copy! bv i v 0 n)
  (cont bv (+ i n) v))

(define-values (read-atom-bv write-atom-bv write-tag-bv read-tag-bv
			     write-integer-bv map-bv)
  (mk 
   read-u8-bv
   read-u16-bv
   read-u32-bv
   read-u64-bv
   read-single-bv
   read-double-bv
   read-copy-bv
   
   write-u8-bv
   write-u16-bv
   write-u32-bv
   write-u64-bv
   write-single-bv
   write-double-bv))

(define (atom-dump-bv scm)
  (write-atom-bv (make-bytevector 4) 0 (lambda (x y) x) scm))

(define (atom-load-bv bv)
  (read-atom-bv bv 0 (lambda (a b c) c) #f))


(define (mk-streamer get-u8 put-u8)
  (let ()
  (define (read-ux-p n read)
    (lambda (port i cont)
      (let ((v (make-bytevector n)))
	(let lp ((j 0))
	  (if (< j n)
	      (begin
		(bytevector-u8-set! v j (get-u8 port))
		(lp (+ j 1)))
	      (cont port (+ i n) (read v 0 endian)))))))

  (define read-u8-p  (lambda (port i cont) (cont port (+ i 1) (get-u8 port))))
  (define read-u16-p (read-ux-p 2 bytevector-u16-ref))
  (define read-u32-p (read-ux-p 4 bytevector-u32-ref))
  (define read-u64-p (read-ux-p 8 bytevector-u64-ref))

  (define read-single-p (read-ux-p 4 bytevector-ieee-single-ref))
  (define read-double-p (read-ux-p 8 bytevector-ieee-double-ref))
  (define (read-copy-p port i cont v n)
    (let lp ((j 0))
      (if (< j n)
	  (begin
	    (bytevector-u8-set! v j (get-u8 port))
	    (lp (+ j 1)))
	  (cont port (+ i n) v))))

  (define (write-u8-p port i cont x)
    (put-u8 port x)
    (cont port (+ i 1)))

  (define (write-u16-p port i cont x)
    (put-u8 port (logand x #xff))
    (put-u8 port (ash (logand x #xff00) -8))
    (cont port (+ i 2)))

  (define (write-u32-p port i cont x)
    (put-u8 port (logand x #xff))
    (put-u8 port (ash (logand x #xff00)     -8))
    (put-u8 port (ash (logand x #xff0000)   -16))
    (put-u8 port (ash (logand x #xff000000) -24))
    (cont port (+ i 4)))
  
  (define (write-u64-p port i cont x)
    (put-u8 port (logand x #xff))
    (put-u8 port (ash (logand x #xff00)     -8))
    (put-u8 port (ash (logand x #xff0000)   -16))
    (put-u8 port (ash (logand x #xff000000) -24))
    (cont port (+ i 4)))

  (define (write-single-p port i cont x)
    (let ((v (make-bytevector 4)))
      (bytevector-ieee-single-set! v 0 x endian)
      (put-u8 port (bytevector-u8-ref v 0))
      (put-u8 port (bytevector-u8-ref v 1))
      (put-u8 port (bytevector-u8-ref v 2))
      (put-u8 port (bytevector-u8-ref v 3))
      (cont port (+ i 4))))

  (define (write-double-p port i cont x)
    (let ((v (make-bytevector 8)))
      (bytevector-ieee-double-set! v 0 x endian)
      (put-u8 port (bytevector-u8-ref v 0))
      (put-u8 port (bytevector-u8-ref v 1))
      (put-u8 port (bytevector-u8-ref v 2))
      (put-u8 port (bytevector-u8-ref v 3))
      (put-u8 port (bytevector-u8-ref v 4))
      (put-u8 port (bytevector-u8-ref v 5))
      (put-u8 port (bytevector-u8-ref v 6))
      (put-u8 port (bytevector-u8-ref v 7))
      (cont port (+ i 4))))


  (define-values (read-atom-p write-atom-p write-tag-p read-tag-p
			      write-integer-p map-p)
    (mk 
     read-u8-p
     read-u16-p
     read-u32-p
     read-u64-p
     read-single-p
     read-double-p
     read-copy-p
   
     write-u8-p
     write-u16-p
     write-u32-p
     write-u64-p
     write-single-p
     write-double-p))

  (define (atom-dump-p scm port)
    (write-atom-p port 0 (lambda (x y) x) scm))

  (define (atom-load-p port)
    (read-atom-p port 0 (lambda (a b c) c) #f))

  (values  read-atom-p
	   write-atom-p
	   write-tag-p
	   read-tag-p
	   write-integer-p

	   map-p
	   
	   atom-dump-p
	   atom-load-p)))


(define-values (read-atom-p
		write-atom-p
		write-tag-p
		read-tag-p
		write-integer-p

		map-p
		
		atom-dump-p
		atom-load-p)
  
  (mk-streamer get-u8 put-u8))

# stis-data
Serializing guile datastructures into binary format

For the repository,

Copyright : Stefan Israelsson Tampe
License   : LGPL v2.0 or later


This library contains serializing and unserializing tools that are flat e.g.
does not buildup any stack and is ideal to use with fibers. The interface
allows various streams to be used and currently we use a bytevector source
and sink and a port as streams. It is quite easy to implement other streams
e.g. based on fiberts for example.

The system is based of three parts, atom.scm, var.scm and mutator.scm.

atom.scm
========
This is a serializing tool to and from streams we also define a port stream 
and a bytevector stream interface. The scheme data structures can't be self
referential and no compression is done for similar tags. This is the fastest
tool.

Example:
> (use-modules (data stis-data atom))

> (define x (iota 100))

> (deifne y (atom-load-bv (atom-write-bv x)))

> (equal? x y)
 #t

var.scm
=======
This code will track all objects with an id and handles all kinds of loops in 
the data staructures. It can also compress strings to the same value and the 
same is true for symbols and keywords. Example:

> (use-modules (data stis-data var)

> (define x (iota 100))

> (equal? x (data-load-bv (data-dumb-bv x)))

  #t



mutator.scm
===========

Source this directory and get going with

> (use-modules (data stis-data mutator))
...
> (push-mutator '(a b c))
...
> (store-mutator "data.dat")
...
> (define obj (load-mutator "data.dat"))
...
> (get-mutator obj)
(a b c)

API
We have either explisit mutator object or an implicit one sitting as a value of the fluid *current-data* the serialisation and deserialisation is working through these mutator objects. The concept is that there is a value that is serialised and that we can refine it by mutatung it. The default is mutator is (make-variable '()) and pushing data with a new value will cons the new value to the list inside this variable and set it. Generally we serialise all set operations which means some sort of functional modifications of the data. Anyhow the renew operation will remove all setters and just store the current state of the mutator variable removing all histore and potentyally compress it.

We redifine som custom setters as
(variable-set! var val)   : make sure to also register rhe new value in
                            the default mutator

(vector-set! mut var val) : make sure to also register the new value in
                            the mutator mut

Similarly is the modifications for the setters,
set-car! set-cdr! vector-set! struct-set! slot-set!

------------------------------------------------------------
mut <- (make-mutator v #:optional (compress? #f))

Make mutator object mut with value object v and if compress? is #t then be agressive in sharing information in the mutator.
------------------------------------------------------------
(store-mutator mut string-or-port)
(store-mutator string-or-port)

If mut is not supplied use default mutator. Store the serialisation in the port or file (via supplied pathstring)
------------------------------------------------------------
mut <- (load-mutator string-or-port)

Load mutator to mut from pathstring or port
------------------------------------------------------------
(renew-mutator)

Will compress the datastructure by skipping the setters and getters and
only directly store the target datastructure
............................................................

(push-mutator mut val)
(push-mutator val)
Push a new val to the val datastructure
-----------------------------------------------------------
val <- (get-mutator mut)
val <- (get-mutator)

Will yield the mutator datastructure
-----------------------------------------------------------
